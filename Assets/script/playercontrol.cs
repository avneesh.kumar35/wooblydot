﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playercontrol : MonoBehaviour {

	Rigidbody2D rb;
	public AudioClip otherClip;
	AudioSource maudio;

	// Use this for initialization
	void Start () {
		rb = transform.root.gameObject.GetComponent<Rigidbody2D> ();
		maudio = GetComponent<AudioSource>();


	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("d")) {
			rb.AddRelativeForce(new Vector2 (5.0f, 50.0f));
			if (!maudio.isPlaying) {
				maudio.Play ();
			}

		}
		int fingerCount = 0;
		foreach (Touch touch in Input.touches) {
			if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
				fingerCount++;

		}
		if (fingerCount > 0) {
			rb.AddRelativeForce (new Vector2 (5.0f, 50.0f));
			if (!maudio.isPlaying) {
				maudio.clip = otherClip;
				maudio.Play ();
			}

		}
		
		if (transform.position.y > 4.66f || transform.position.y < -4.5) {
			Destroy (transform.root.gameObject);
			StoreHighScore (pointCounter.score);
			pointCounter.score = 0;
			SceneManager.LoadScene ("title");
		}

	
	}

	void StoreHighScore(int Score){
		int oldHighScore = PlayerPrefs.GetInt ("highscore",0);
		if (Score > oldHighScore) {
			if(googleplayscript.isAuthenticated){
				Social.ReportScore (Score,"CgkIlPeT67oWEAIQAQ",(bool success) => {
					// handle success or failure
					if(success){
						Debug.Log("Score posted successfully");
					}
					else{
						Debug.Log("Not able to post score");
					}

				});
				if(Score>=10){
					Social.ReportProgress ("CgkIlPeT67oWEAIQAg",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}
				if(Score>=25){
					Social.ReportProgress ("CgkIlPeT67oWEAIQAw",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}
				if(Score>=50){
					Social.ReportProgress ("CgkIlPeT67oWEAIQBA",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}
				if(Score>=75){
					Social.ReportProgress ("CgkIlPeT67oWEAIQBQ",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}
				if (Score >= 100) {
					Social.ReportProgress ("CgkIlPeT67oWEAIQBg", 100.0f, (bool success) => {
						if (success) {
							Debug.Log ("Score posted successfully");
						} else {
							Debug.Log ("Not able to post score");
						}
					});
				}
			}
			PlayerPrefs.SetInt ("highscore", Score);
		}
	}
}
