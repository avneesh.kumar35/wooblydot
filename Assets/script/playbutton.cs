﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GoogleMobileAds.Api;
using System.IO;
using System.Runtime.InteropServices;

public class playbutton : MonoBehaviour {

	// Use this for initialization
	public Text HighscoreText;
	static int clickcount=0;
	string adUnitId = "ca-app-pub-9408156079023384/2074107353";
	InterstitialAd interstitial;
	AdRequest request;
	string subject = "My High Score";
	string body = "I challenge you to bet my high score:"+PlayerPrefs.GetInt ("highscore",0).ToString()+". If you don't have this awsome game download it at https://play.google.com/store/apps/details?id=capricornus.app2.wobblydot";
	void Start () {
		HighscoreText.text = HighscoreText.text + ": " + PlayerPrefs.GetInt ("highscore", 0).ToString ();
		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
		if (clickcount % 3 == 2) {
			ShowInterstitial ();
		}


	}

	public void ChangeScene(){

		clickcount++;

		SceneManager.LoadScene ("scene1");

	}

	public void showleaderboard(){
		if (googleplayscript.isAuthenticated) {
			Social.ShowLeaderboardUI ();
		}
	}
	public void showAchievements(){
		if (googleplayscript.isAuthenticated) {
			Social.ShowAchievementsUI ();
		}
	}

	public void shareScore(){
		StartCoroutine(ShareAndroidText());
	}
	IEnumerator ShareAndroidText()
	{
		yield return new WaitForEndOfFrame();
		//execute the below lines if being run on a Android device
		#if UNITY_ANDROID
		//Reference of AndroidJavaClass class for intent
		AndroidJavaClass intentClass = new AndroidJavaClass ("android.content.Intent");
		//Reference of AndroidJavaObject class for intent
		AndroidJavaObject intentObject = new AndroidJavaObject ("android.content.Intent");
		//call setAction method of the Intent object created
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		//set the type of sharing that is happening
		intentObject.Call<AndroidJavaObject>("setType", "text/plain");
		//add data to be passed to the other activity i.e., the data to be sent
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), "Text Sharing ");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);
		//get the current activity
		AndroidJavaClass unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
		//start the activity by sending the intent data
		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
		currentActivity.Call("startActivity", jChooser);
		#endif
	}


	public void Exit(){
		Debug.Log ("Exiting...");
		Application.Quit ();
	}

	void ShowInterstitial(){
		while (!interstitial.IsLoaded ()) {
		}
		if (interstitial.IsLoaded())
		{
			interstitial.Show();
		} else    {
			Debug.Log ("Interstitial is not ready yet.");
		}    
	}

}
