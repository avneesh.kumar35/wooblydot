﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class googleplayscript : MonoBehaviour {

	public int highscore;
	public static bool isAuthenticated=false;
	void Awake(){
	
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate ();
	}
	// Use this for initialization
	void Start () {
		
		Social.localUser.Authenticate((bool success) => {
			// handle success or failure
			if(success){
				isAuthenticated=true;
			}

		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
