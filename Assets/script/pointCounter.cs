﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pointCounter : MonoBehaviour {
	public Text scoreText;
	public static int score;

	void onStart(){
		score = 0;

	}
	void OnTriggerExit2D(Collider2D col){
		if (col.tag == "GameController") {
			AddScore (1);
			UpdateScore ();
			//Debug.Log (score.ToString ());
		}
	}

	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;

		UpdateScore ();
	}

	void UpdateScore ()
	{
		scoreText.text = "Score: " + score;

	}
	void StoreHighScore(int Score){
		int oldHighScore = PlayerPrefs.GetInt ("highscore",0);
		if (Score > oldHighScore) {
			PlayerPrefs.SetInt ("highscore", score);
		}
	}

}
