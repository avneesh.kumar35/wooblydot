﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class hurdles : MonoBehaviour {

	// Use this for initialization


	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
	void OnCollisionEnter2D(Collision2D coll){
		if(coll.gameObject.tag=="GameController"){
			Destroy (coll.gameObject);
			StoreHighScore (pointCounter.score);
			pointCounter.score = 0;
			SceneManager.LoadScene ("title");
		}
	}
	void StoreHighScore(int Score){
		int oldHighScore = PlayerPrefs.GetInt ("highscore",0);
		if (Score > oldHighScore) {
			if(googleplayscript.isAuthenticated){
				Social.ReportScore (Score,"CgkIlPeT67oWEAIQAQ",(bool success) => {
					// handle success or failure
					if(success){
						Debug.Log("Score posted successfully");
					}
					else{
						Debug.Log("Not able to post score");
					}

				});
				if(Score>=10){
					Social.ReportProgress ("CgkIlPeT67oWEAIQAg",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}
				if(Score>=25){
					Social.ReportProgress ("CgkIlPeT67oWEAIQAw",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}
				if(Score>=50){
					Social.ReportProgress ("CgkIlPeT67oWEAIQBA",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}
				if(Score>=75){
					Social.ReportProgress ("CgkIlPeT67oWEAIQBQ",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}
				if(Score>=100){
					Social.ReportProgress ("CgkIlPeT67oWEAIQBg",100.0f,(bool success)=>{
						if(success){
							Debug.Log("Score posted successfully");
						}
						else{
							Debug.Log("Not able to post score");
						}
					});
				}

			}
			PlayerPrefs.SetInt ("highscore",Score);
		}
	}
}
